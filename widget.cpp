#include "widget.h"

Widget::Widget(QWidget *parent) : QQuickWidget(parent)
{
    setSource(QUrl("qrc:/qml/MainWindow.qml"));
    setResizeMode(QQuickWidget::SizeRootObjectToView);
    setMinimumSize(1000, 1000);


    init_game();
    game_timer = new QTimer;
    game_timer->setInterval(0);
    connect(game_timer, SIGNAL(timeout()), this, SLOT(play()));
    game_timer->start();

    qDebug() << "Generate";




    update_matrix();
}

Widget::~Widget()
{

}

void Widget::set_cell_color(int x, int y, QString color)
{
    QMetaObject::invokeMethod(rootObject(), "set_cell_color",
                              Q_ARG(QVariant, x),
                              Q_ARG(QVariant, y),
                              Q_ARG(QVariant, color));
}

void Widget::play()
{
    static int it_counter = 0;
    static int counter = 1;
    counter = counter % 100;
    if(!counter)
        spawn_food();
    counter ++;

    qDebug() << it_counter ++;
    iteration();
    update_matrix();
}

void Widget::init_game()
{
    bait_matrix[50][50].init(Type::Male);
    bait_matrix[50][48].init(Type::Female);
    spawn_food();
    update_matrix();
    qDebug() << "Game started";
}

void Widget::spawn_food()
{
    int food_number = qrand() % 90 + 10;
    int food_count_down = food_number * food_number;
    while (food_number && food_count_down)
    {
        int x = qrand() % 97;
        int y = qrand() % 97;
        if(bait_matrix[x][y].get_type() == Type::Empty)
        {
            bait_matrix[x][y].init(Type::Food);
            food_number --;
        }
        food_count_down --;
    }
}

void Widget::iteration()
{
    for(int i = 0; i < FIELD_WIDTH; i++)
        for(int j = 0; j < FIELD_HEIGHT; j++)
            if((bait_matrix[i][j].get_type() == Type::Male ||
                bait_matrix[i][j].get_type() == Type::Female) &&
                !bait_matrix[i][j].activated())
            {
                QList<QPoint> available_points_list = get_available_points_list(i, j);  // check borders
                QList<QPoint> filtered_points_list = filter_gender(available_points_list,
                                                                   bait_matrix[i][j].get_type());
                if(filtered_points_list.length() > 0)
                {
                    int current_point = qrand() % filtered_points_list.length();
                    QPoint step_point = filtered_points_list.at(current_point);

                    // Check_food
                    if(bait_matrix[step_point.x()][step_point.y()].get_type() == Type::Food)
                    {
                       bait_matrix[step_point.x()][step_point.y()] = bait_matrix[i][j];
                       bait_matrix[step_point.x()][step_point.y()].feed();
                       bait_matrix[step_point.x()][step_point.y()].step();
                       bait_matrix[i][j].clear();
                       continue;
                    }
                    // Check another gender
                    static const int spawn_radius = 2;
                    if(bait_matrix[step_point.x()][step_point.y()].get_type() != bait_matrix[i][j].get_type())
                    {
                        bait_matrix[i][j].step();
                        bait_matrix[step_point.x()][step_point.y()].step();

                        int first_food_number = bait_matrix[step_point.x()][step_point.y()].get_food_counter();
                        int second_food_number = bait_matrix[i][j].get_food_counter();

                        if(first_food_number <= 10 || second_food_number <= 10)
                            spawn_baits(i, j, spawn_radius, 1);
                        else
                            if(first_food_number < 20 || second_food_number < 20)

                            spawn_baits(i, j, spawn_radius, 2);
                            else
                                if(first_food_number < 40 || second_food_number < 40)

                                    spawn_baits(i, j, spawn_radius, 3);
                                else
                                    spawn_baits(i, j, spawn_radius, 4);
                    }

                    if(bait_matrix[step_point.x()][step_point.y()].get_type() == Type::Empty)
                    {
                        bait_matrix[step_point.x()][step_point.y()] = bait_matrix[i][j];
                        bait_matrix[step_point.x()][step_point.y()].step();
                        bait_matrix[i][j].clear();
                    }
                }
            }

    reset_activation();
}

void Widget::update_matrix()
{
    for(int i = 0; i < FIELD_WIDTH; i++)
        for(int j = 0; j < FIELD_HEIGHT; j++)
            set_cell_color(i, j, bait_matrix[i][j].get_color());
}

bool Widget::available_cell(QPoint point)
{
    bool result = true;
    int x = point.x();
    int y = point.y();

    if(x > FIELD_WIDTH || x < 0)
        result = false;

    if(y > FIELD_HEIGHT || y < 0)
        result = false;

    return result;
}

/**
 * @brief Widget::get_available_points_list
 * @param x
 * @param y
 * @return
 * Checks available points
 */
QList<QPoint> Widget::get_available_points_list(int x, int y)
{
    QList <QPoint> points_list;
    points_list.append({x + 1, y});
    points_list.append({x - 1, y});
    points_list.append({x, y + 1});
    points_list.append({x, y - 1});

    int points_iterator = 0;
    foreach (QPoint point, points_list)
        if(!available_cell(point))
            points_list.removeAt(points_iterator);
        else
            points_iterator++;

    return points_list;
}

/**
 * @brief Widget::filter_gender
 * @param points_list
 * @param type
 * @return
 */
QList<QPoint> Widget::filter_gender(QList<QPoint> points_list, Type type)
{
    QList <QPoint> result_list = points_list;
    int points_iterator = 0;
    foreach (QPoint point, result_list)
        if(bait_matrix[point.x()][point.y()].get_type() == type)
            result_list.removeAt(points_iterator);
        else
            points_iterator ++;

    return result_list;
}

void Widget::reset_activation()
{
    for(int i = 0; i < FIELD_WIDTH; i++)
        for(int j = 0; j < FIELD_HEIGHT; j++)
            bait_matrix[i][j].reset_activation();
}

void Widget::spawn_baits(int x, int y, int radius, int number)
{
    int spawned_number = 0;
    int tryout_number = radius * radius;
    while (spawned_number < number && tryout_number > 0)
    {
        int current_x = (qrand() % 2 ? -1 : 1) * (qrand() % radius) + x;
        int current_y = (qrand() % 2 ? -1 : 1) * (qrand() % radius) + y;

        if(current_x > 5 && current_y > 5 && current_x < 90 && current_y < 90)
        {
//            qDebug() << current_x << " " << current_y;
            if(bait_matrix[current_x][current_y].get_type() == Type::Empty)
            {
                bait_matrix[current_x][current_y].init();
                bait_matrix[current_x][current_y].step();
                spawned_number++;
            }
        }


        tryout_number--;
    }
}









