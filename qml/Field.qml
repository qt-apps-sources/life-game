import QtQuick 2.0
import "qrc:/qml"

Item
{
    id: root
    height: grid_view.height
    width: grid_view.width

    property var rows_number: 100
    property var columns_number: 100
    property var spacing: 2
    GridView
    {

       id: grid_view

       cellWidth: 8 + spacing
       cellHeight: 8 + spacing

       width: columns_number * cellWidth
       height:  rows_number * cellHeight

       interactive: false
       model: dataModel
       flow: GridView.FlowLeftToRight

       delegate: Bait
       {
            color: model.color
            width: model.width
            height: model.height
       }

       Component.onCompleted:
       {
            for(var i = 0; i < columns_number * rows_number; i++)
               dataModel.append({color: "#ff00ff",
                                 height: grid_view.cellHeight - spacing,
                                 width: grid_view.cellWidth - spacing});
       }

       ListModel
       {
           id: dataModel
       }
    }

    function set_cell_color(x, y, color)
    {
        dataModel.setProperty(y * rows_number + x, "color", color);
    }

}
