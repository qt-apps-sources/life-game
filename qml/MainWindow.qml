import QtQuick 2.0

import "qrc:/qml"

Item {
    id: root

    Rectangle
    {
        id: background
        anchors.fill: parent
        color: "#000000"
    }

    Field
    {
        id: game_field
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }


    function set_cell_color(x, y, color)
    {
        game_field.set_cell_color(x, y, color);
    }
}
