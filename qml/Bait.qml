import QtQuick 2.0

Item
{
    id: root

    property var color : "#ffffff"
    height: 5
    width: 5
    Rectangle
    {
        id: type
        height: root.height
        width: root.width
        color: root.color
        radius: color == "#00ff00" ? 4 : 0
    }

    function set_color(color)
    {
        root.color = color;
    }
}
