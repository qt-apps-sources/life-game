#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QQuickWidget>
#include <QQuickItem>
#include "elements/Bait/bait.h"
#include <QThread>
#include <QTimer>

#define FIELD_WIDTH 100
#define FIELD_HEIGHT 100

class Widget : public QQuickWidget
{
    Q_OBJECT
private:
    Bait bait_matrix[FIELD_WIDTH][FIELD_HEIGHT];
    QTimer *game_timer;

    void set_cell_color(const int x, const int y, const QString color);
    void init_game();
    void iteration();
    void reset_activation();
    void update_matrix();
    void spawn_food();
    void spawn_baits(int x, int y, int radius, int number);


    bool available_cell(const QPoint point);
    QList<QPoint> get_available_points_list(const int x, const int y);
    QList<QPoint> filter_gender(const QList<QPoint> points_list, const Type type);

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void play();
};

#endif // WIDGET_H
