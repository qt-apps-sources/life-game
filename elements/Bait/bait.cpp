#include "bait.h"

Bait::Bait()
{
    this->type = Type::Empty;
    this->color = "#ffffff";
    activate = false;
}
// Initialization methods
void Bait::init()
{
    init_gender();
    life_counter = LIFE_STEPS_NUMBER;
    food_counter = FOOD_START_NUMBER;
}

void Bait::init(Type gender)
{
    this->type = gender;

    switch(gender)
    {
        case Female:
            color = "#F40D0D";
            break;
        case Male:
            color = "#2AC4FF";
            break;
        case Food:
            color = "#00FF00";
            break;
        case Empty:
            color = "#FFFFFF";
            break;
    }

    life_counter = LIFE_STEPS_NUMBER;
    food_counter = FOOD_START_NUMBER;
}

void Bait::init_gender()
{
    int gender = qrand() % 2;
    this->type = static_cast<Type>(gender);
    if(this->get_type() == Type::Male)
        color = "#2AC4FF";
    else
        color = "#F40D0D";
}

// Play methods
void Bait::step()
{
    activate = true;
    if(life_counter == 0 || food_counter == 0)
        clear();
    else
    {
        food_counter = food_counter - 1;
        life_counter = life_counter - 1;
    }
}

void Bait::feed()
{
    if(food_counter < MAX_FOOD_SIZE)
        food_counter += FEED_SIZE;

    if(food_counter > MAX_FOOD_SIZE)
        food_counter = MAX_FOOD_SIZE;
}

void Bait::clear()
{
    this->food_counter = 0;
    this->life_counter = 0;
    this->type = Type::Empty;
    this->color = "#ffffff";
}
