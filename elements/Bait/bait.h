#ifndef BAIT_H
#define BAIT_H

#include <QObject>

#define LIFE_STEPS_NUMBER 500
#define FOOD_START_NUMBER 20

#define FEED_SIZE 20
#define MAX_FOOD_SIZE 100

enum Type
{
    Male = 0,
    Female,
    Empty,
    Food
};

class Bait
{
private:
    int life_counter;
    int food_counter;
    void init_gender();


    Type type;
    QString color;
    bool activate;
public:
    Bait();
    void init();
    void init(Type gender);
    void clear();
    void feed();
    void step();
    void set_food(int food){this->food_counter = food;}
    void reset_activation(){activate = false;}
    bool activated(){return this->activate;}

    // Get
    int get_life_counter(){return life_counter;}
    int get_food_counter(){return  food_counter;}

    QString get_color() {return color;}
    Type get_type(){return type;}
};

#endif // BAIT_H
